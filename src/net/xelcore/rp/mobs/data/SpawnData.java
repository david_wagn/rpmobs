package net.xelcore.rp.mobs.data;

import net.xelcore.rp.mobs.object.MobSpawner;
import net.xelcore.rp.mobs.object.Mobs;
import net.xelcore.rpcore.data.MySQLData;
import net.xelcore.rpcore.main.Main;
import org.bukkit.entity.EntityType;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SpawnData {

    public static void init() {
        Main.log.info(" ");
        Main.log.info("----------[Mobloader]----------");
        Main.log.info("Loading mobs...");
        loadMobs();
        Main.log.info("Loaded " + MobSpawner.spawners.size() + " mobs");
        Main.log.info("----------[Mobloader]----------");
        Main.log.info(" ");
    }

    private static void loadMobs() {
        try {
            final ResultSet rs = MySQLData.get("mobspawns", null);
            while (rs.next()) {
                new MobSpawner(Mobs.valueOf(rs.getString("mob")), EntityType.valueOf(rs.getString("replace")), rs.getInt("chance"));
                Main.log.info("Loaded mob " + rs.getString("mob") + ", it has a " + rs.getInt("chance") + "% chance to replace " + rs.getString("replace"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
