package net.xelcore.rp.mobs.mobs;

import de.slikey.effectlib.effect.CylinderEffect;
import net.xelcore.rp.mobs.main.Main;
import net.xelcore.rp.mobs.object.MobBuilder;
import net.xelcore.rpcore.main.Utils;
import net.xelcore.rplib.itemstack.ItemStackBuilder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.metadata.FixedMetadataValue;

public class FierySkeleton implements Listener {

    public FierySkeleton(Location loc) {
        new MobBuilder(loc, EntityType.SKELETON) {

            @Override
            public void init() {
                Skeleton s = (Skeleton) getEntity();
                s.getEquipment().setItemInMainHand(new ItemStackBuilder(Material.BOW).addEnchantment(Enchantment.ARROW_FIRE, 2).build());
                s.setCustomNameVisible(true);
                s.setCustomName("§6Fiery Skeleton");
                s.setMetadata("FIERY_SKELETON", new FixedMetadataValue(Main.instance, "true"));
                setEntity(s);
                CylinderEffect c = new CylinderEffect(Main.effectManager);
                c.setEntity(getEntity());
                c.iterations = -1;
                c.particles = 10;
                c.disappearWithOriginEntity = true;
                c.start();
                useTimer(true, 1000L);
            }

            @Override
            public void timer() {
                for(Entity e : Utils.getNearbyEntities(getEntity().getLocation(), 5)) {
                    if(e instanceof Player) {
                        Player p = (Player) e;
                        p.setFireTicks(60);
                    }
                }
            }

            @Override
            public void entityDamage(EntityDamageEvent e) {
                if(e.getCause() == EntityDamageEvent.DamageCause.FIRE || e.getCause() == EntityDamageEvent.DamageCause.FIRE_TICK) e.setCancelled(true);
            }

            @Override
            public void entityDeath(EntityDeathEvent e) {
            }

            @Override
            public void entityTarget(EntityTargetEvent e) {
                e.setCancelled(false);
            }

            @Override
            public void bowShoot(EntityShootBowEvent e) {

            }

            @Override
            public void projectileHit(ProjectileHitEvent e) {
                if(e.getHitEntity() instanceof Player) {
                    e.getHitEntity().setFireTicks(100);
                }
            }


            @Override
            public void explode(EntityExplodeEvent e) {

            }

            @Override
            public void creeperPower(CreeperPowerEvent e) {

            }
        };
    }

}
