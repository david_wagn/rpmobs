package net.xelcore.rp.mobs.object;

import net.xelcore.rp.mobs.main.Main;
import net.xelcore.rp.mobs.mobs.FierySkeleton;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

public class MobSummoner {

    private Mobs mob;
    private Location location;

    public MobSummoner(Mobs mob) {
        this.mob = mob;
    }

    public void spawn(Location loc, boolean natural) {
        this.location = loc;
        Main.log.debug("rpmobs>MobSummoner", (natural ? "Natural" : "Custom") + " spawn of " + mob.toString().toUpperCase() + " at " + Math.round(loc.getX()) + "/" + Math.round(loc.getY()) + "/" + Math.round(loc.getZ()));
        switch (this.mob) {
            case FIERY_SKELETON:
                new FierySkeleton(loc);
                break;
            case ANCIENT_GOLEM:
                break;
        }
    }

    public ItemStack getSpawner() {
        return null;
    }

    public Mobs getMob() {
        return mob;
    }

    public Location getLoc() {
        return location;
    }
}
