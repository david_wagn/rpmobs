package net.xelcore.rp.mobs.object;

import de.slikey.effectlib.Effect;
import de.slikey.effectlib.util.ParticleEffect;
import net.minecraft.server.v1_15_R1.Entity;
import net.minecraft.server.v1_15_R1.World;
import net.minecraft.server.v1_15_R1.WorldServer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_15_R1.CraftWorld;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

@SuppressWarnings("unused")
public abstract class MobBuilder implements Listener {

    public static Plugin instance;
    public static ArrayList<LivingEntity> entities = new ArrayList<>();

    public static void setPlugin(Plugin arg0) {
        instance = arg0;
    }

    private void registerEvents() {
        if(instance != null) {
            Bukkit.getPluginManager().registerEvents(this, instance);
        } else {
            throw new NullPointerException("Instance has not been set");
        }
    }

    private LivingEntity entity;
    private Timer timer;
    private boolean useTimer;
    private long interval;

    public MobBuilder(Location loc, EntityType type) {
        registerEvents();
        this.entity = (LivingEntity) loc.getWorld().spawnEntity(loc, type);
        init();
        if(this.useTimer) {
            this.timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    if(getEntity().isDead()) {
                        this.cancel();
                        return;
                    }
                    timer();
                }
            }, 100, interval);
        }
        entities.add(this.entity);
    }

    public void spawn() {
        init();
    }

    public LivingEntity getEntity() {
        return entity;
    }

    public void setEntity(LivingEntity e) {
        this.entity = e;
    }

    public void useTimer(boolean use, long interval) {
        useTimer = use;
        this.interval = interval;
    }

    public abstract void init();

    public abstract void timer();

    public abstract void entityDamage(EntityDamageEvent e);

    public abstract void entityDeath(EntityDeathEvent e);

    public abstract void entityTarget(EntityTargetEvent e);

    public abstract void bowShoot(EntityShootBowEvent e);

    public abstract void projectileHit(ProjectileHitEvent e);

    public abstract void explode(EntityExplodeEvent e);

    public abstract void creeperPower(CreeperPowerEvent e);

    @EventHandler
    public final void onEntityDamage(EntityDamageEvent e) {
        if(e.getEntity() == this.entity) {
            entityDamage(e);
        }
    }

    @EventHandler
    public final void onEntityDeath(EntityDeathEvent e) {
        if(e.getEntity() == this.entity) {
            this.timer.cancel();
            entityDeath(e);
        }
    }

    @EventHandler
    public final void onEntityTarget(EntityTargetEvent e) {
        if(e.getEntity() == this.entity) {
            entityTarget(e);
        }
    }

    @EventHandler
    public final void onBowShoot(EntityShootBowEvent e) {
        if(e.getEntity() == this.entity) {
            bowShoot(e);
        }
    }

    @EventHandler
    public final void onProjectileHit(ProjectileHitEvent e) {
        if(e.getEntity().getShooter() == getEntity()) {
            projectileHit(e);
        }
    }

    @EventHandler
    public final void onExplode(EntityExplodeEvent e) {
        if(e.getEntity() == this.entity) {
            explode(e);
        }
    }

    @EventHandler
    public final void onCreeperPower(CreeperPowerEvent e) {
        if(e.getEntity() == this.entity) {
            creeperPower(e);
        }
    }

}
