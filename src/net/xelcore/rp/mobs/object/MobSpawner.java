package net.xelcore.rp.mobs.object;

import org.bukkit.entity.EntityType;

import java.util.ArrayList;

public class MobSpawner {

    private Mobs mob;
    private EntityType replace;
    private Integer chance;

    public static ArrayList<MobSpawner> spawners = new ArrayList<>();

    public MobSpawner(Mobs mob, EntityType replace, Integer chance) {
        this.mob = mob;
        this.replace = replace;
        this.chance = chance;
        spawners.add(this);
    }

    public Mobs getMob() {
        return mob;
    }

    public EntityType getReplace() {
        return replace;
    }

    public Integer getChance() {
        return chance;
    }
}
