package net.xelcore.rp.mobs.command;

import net.xelcore.rp.mobs.main.Main;
import net.xelcore.rp.mobs.object.MobSummoner;
import net.xelcore.rp.mobs.object.Mobs;
import net.xelcore.rplib.message.ClickableMessage;
import net.xelcore.rplib.message.Message;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpawnCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player) sender;
            if(p.hasPermission("rp.mobs.spawn")) {
                if(args.length == 1) {
                    if(args[0].equals("list")) {
                        new Message(p).prefix("Erschaffbare mobs");
                        for(Mobs mob : Mobs.values()) {
                            new ClickableMessage(p, "§7" + mob.toString().toLowerCase() + " §8- ", "§aSPAWN", "spawn " + mob.toString().toLowerCase());
                        }
                    } else {
                        boolean contains = false;
                        Mobs mob = null;
                        for(Mobs m : Mobs.values()) {
                            if(m.toString().toLowerCase().equals(args[0].toLowerCase())) {
                                contains = true;
                                mob = m;
                            }
                        }
                        if(contains) {
                            new MobSummoner(mob).spawn(p.getLocation(), false);
                            new Message(p).prefix("Du hast " + mob.toString().toUpperCase() + " erschaffen");
                        } else {
                            new Message(p).errorprefix("Dieser Mob existiert nicht, tippe §6/spawn list§7 um eine liste and Mobs zu sehen");
                        }
                    }
                } else {
                    new Message(p).wrongcmdusage("/spawn <MobID/list>");
                }
            }
        } else {
            Main.log.error("Only players can execute this command");
        }

        return true;
    }
}
