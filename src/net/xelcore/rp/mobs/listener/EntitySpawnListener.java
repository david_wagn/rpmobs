package net.xelcore.rp.mobs.listener;

import net.xelcore.rp.mobs.object.MobSpawner;
import net.xelcore.rp.mobs.object.MobSummoner;
import net.xelcore.rpcore.main.Utils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

public class EntitySpawnListener implements Listener {

    @EventHandler
    public void entitySpawn(EntitySpawnEvent e) {
        for(MobSpawner m : MobSpawner.spawners) {
            if(e.getEntityType() == m.getReplace()) {
                if(Utils.chance(m.getChance())) {
                    e.getEntity().remove();
                    new MobSummoner(m.getMob()).spawn(e.getLocation(), true);
                }
            }
        }
    }
}
