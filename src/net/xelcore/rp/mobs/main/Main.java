package net.xelcore.rp.mobs.main;

import de.slikey.effectlib.EffectManager;
import net.xelcore.rp.mobs.command.SpawnCommand;
import net.xelcore.rp.mobs.data.SpawnData;
import net.xelcore.rp.mobs.listener.EntitySpawnListener;
import net.xelcore.rp.mobs.object.MobBuilder;
import net.xelcore.rplib.logger.Logger;
import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    public static Logger log;
    public static Main instance;
    public static EffectManager effectManager;

    @Override
    public void onEnable() {
        instance = this;
        log = net.xelcore.rplib.main.Main.log;
        effectManager = new EffectManager(this);
        MobBuilder.setPlugin(this);

        SpawnData.init();

        Bukkit.getPluginManager().registerEvents(new EntitySpawnListener(), this);

        this.getCommand("spawn").setExecutor(new SpawnCommand());

    }

    @Override
    public void onDisable() {
        for(LivingEntity e : MobBuilder.entities) {
            e.remove();
        }
        effectManager.dispose();
    }
}
